# stuff-bot
--------------

#### A basic bot for Stuff.co.nz on Facebook Messenger Platform

install dependencies
```
$ npm install
```
create a local self-signed certificate
```
$ cd path/to/root/directory
$ openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days XXX -nodes
```
answer all the questions, then you can start server
```
$ npm run start
```
navigate to ```https://localhost:3000/status```. Chrome will ask if you want to continue, say yes by clicking ```Advanced```.

run tests
```
npm run tests
```