'use strict'
const express = require('express')
const bodyParser = require('body-parser')

module.exports = function (bot) {
  bot.on('error', (err) => {
    console.log(err.message)
  })

  bot.on('message', (payload, reply) => {
    let text = payload.message.text

    bot.getProfile(payload.sender.id, (err, profile) => {
      if (err) throw err

      reply({ text }, (err) => {
        if (err) throw err

        console.log(`Echoed back to ${profile.first_name} ${profile.last_name}: ${text}`)
      })
    })
  })

  let app = express()

  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({
    extended: true
  }))

  app.get('/', (req, res) => {
    return bot._verify(req, res)
  })

  app.get('/status', (req, res) => {
    res.end(JSON.stringify({status: 'ok'}))
  })

  app.post('/', (req, res) => {
    bot._handleMessage(req.body)
    res.end(JSON.stringify({status: 'ok'}))
  })

  return app
}
