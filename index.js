'use strict'
const https = require('https')
const fs = require('fs')
const middleware = require('./src/middleware.js')
const Bot = require('messenger-bot')

const port = 3000

let bot = new Bot({
  token: 'PAGE_TOKEN',
  verify: 'VERIFY_TOKEN',
  app_secret: 'APP_SECRET'
})

const app = middleware(bot)

https.createServer({
  key: fs.readFileSync(process.env.PRIVKEY || './key.pem'),
  cert: fs.readFileSync(process.env.CERT || './cert.pem')
}, app).listen(port, () => {
  console.log(`Listening on port ${port}...`)
})
